import {
    FaHome,
    FaList,
    FaUserFriends,
} from "react-icons/fa";
export const listGender = [
    { name: "Male", key: 0 },
    { name: "Female", key: 1 }
];
export const listNav = [
    { name: "Edit Profile", key: 1 },
    // { name: "Change Profile Picture", key: 2 },
    { name: "Update Password", key: 3 }
];
export const listMenu = [
    { name: "Home", icon: <FaHome />, link: "" },
    { name: "List Project", icon: <FaList />, link: "" },
    { name: "Collaboration", icon: <FaUserFriends />, link: "" }
];